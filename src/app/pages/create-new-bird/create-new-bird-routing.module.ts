import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateNewBirdComponent } from './create-new-bird.component';

const routes: Routes = [
  {
    path: '',
    component: CreateNewBirdComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreateNewBirdRoutingModule { }
