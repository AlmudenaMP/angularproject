import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateNewBirdComponent } from './create-new-bird.component';

describe('CreateNewBirdComponent', () => {
  let component: CreateNewBirdComponent;
  let fixture: ComponentFixture<CreateNewBirdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateNewBirdComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CreateNewBirdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
