import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreateNewBirdRoutingModule } from './create-new-bird-routing.module';
import { CreateNewBirdComponent } from './create-new-bird.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    CreateNewBirdComponent
  ],
  imports: [
    CommonModule,
    CreateNewBirdRoutingModule,
    ReactiveFormsModule
  ]
})
export class CreateNewBirdModule { }
