import { Component } from '@angular/core';
import { NationalPark } from 'src/app/core/services/birds/api/api-birds.models';
import {  parks } from 'src/app/core/services/birds/birds.data';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms'
import { ActivatedRoute, Router } from '@angular/router';
import { switchMap, map, of } from 'rxjs';
import { Bird } from 'src/app/core/services/birds/birds.models';
import { BirdsService } from 'src/app/core/services/birds/birds.service';

@Component({
  selector: 'app-create-new-bird',
  templateUrl: './create-new-bird.component.html',
  styleUrls: ['./create-new-bird.component.scss']
})
export class CreateNewBirdComponent {

  public birdForm?: FormGroup;
  public parks: NationalPark[] = parks;
  public canEdit: boolean = false;
  public birdId?: string;
  public isBirdCreated: boolean = false;


  constructor(
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private birdsService: BirdsService,
    private router: Router
  ) {
    this.activatedRoute.queryParams.subscribe((queryParams => {

    }));

    this.activatedRoute.queryParams.pipe(
      map((queryParams) => queryParams['id']),
      switchMap((id: string) => {
        if (!id) {
          return of(undefined);
        } else {
          this.birdId = id;
          return this.birdsService.getNewBirdDetail(id);
        }
      })
    ).subscribe((bird?: Bird) => {
      this.canEdit = !!bird;
      this.createNewForm(bird);
    })


    this.birdForm = this.fb.group({
      family: new FormControl('', [Validators.required, Validators.maxLength(20)]),
      binomialName: new FormControl('', [Validators.required, Validators.maxLength(40)]),
      commonName: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required, Validators.minLength(60)]),
      food: new FormControl('', [Validators.required]),
      redList: new FormControl('', [Validators.required]),
      seen: new FormControl('', [Validators.required]),
      img: new FormControl('', [Validators.required])
    });
  }

  public createNewForm(bird?: Bird) {
    this.birdForm = this.fb.group({
      family: new FormControl(bird?.family || '', [Validators.required, Validators.maxLength(20)]),
      binomialName: new FormControl(bird?.binomialName ||'', [Validators.required, Validators.maxLength(40)]),
      commonName: new FormControl(bird?.commonName ||'', [Validators.required]),
      description: new FormControl(bird?.description ||'', [Validators.required, Validators.minLength(60)]),
      food: new FormControl(bird?.food ||'', [Validators.required]),
      redList: new FormControl(bird?.redList ||'', [Validators.required]),
      seen: new FormControl(bird?.seen ||'', [Validators.required]),
      img: new FormControl(bird?.img ||'', [Validators.required])
    });
  }

  public createNewBird() {   
    if (!this.birdForm?.valid) { return; }
    const birdRequest = this.canEdit && this.birdId
      ? this.birdsService.editNewBird(this.birdId, this.birdForm?.value)
      : this.birdsService.createNewBird(this.birdForm?.value);
    birdRequest.subscribe(() => {
      this.isBirdCreated = true;
      this.birdForm?.reset();
      this.router.navigate(['my-creations']);
    });
  }
}
