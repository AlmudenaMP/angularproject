import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BirdListComponent } from './bird-list.component';
import { BirdComponent } from './components/bird/bird.component';
import { BirdListRoutingModule } from './bird-list.routing.module';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    BirdListComponent,
    BirdComponent
  ],
  imports: [
    CommonModule,
    BirdListRoutingModule,
    RouterModule,
    SharedModule,
    FormsModule
  ],
  exports: [
    BirdListComponent,
    BirdComponent
  ]
})
export class BirdListModule { }
