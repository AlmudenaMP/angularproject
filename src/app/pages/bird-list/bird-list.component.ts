import { Component, OnInit } from '@angular/core';
import { Bird } from 'src/app/core/services/birds/birds.models';
import { BirdsService } from 'src/app/core/services/birds/birds.service';
import { switchMap } from 'rxjs';



@Component({
  selector: 'app-bird-list',
  templateUrl: './bird-list.component.html',
  styleUrls: ['./bird-list.component.scss']
})
export class BirdListComponent implements OnInit {

  public birds: Bird[] = [];
  public birdCommonName: string = '';

  constructor(private birdsService: BirdsService) {}

  public ngOnInit(): void {
    this.birdsService.getBirds().subscribe((birdsFromApi) => {
      this.birds = birdsFromApi;
    });
  }

  public removeBirdFromList(id?: string) {
    if (!id) { return; }
    this.birdsService.deleteBird(id).pipe(
      switchMap((bird) => {
        return this.birdsService.getBirds();
      })
    ).subscribe((birdsFromApi) => {
      this.birds = birdsFromApi;
    });        
  } 
}
