import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Bird } from 'src/app/core/services/birds/birds.models';

@Component({
  selector: 'app-bird',
  templateUrl: './bird.component.html',
  styleUrls: ['./bird.component.scss']
})
export class BirdComponent {

  @Input() public bird?: Bird;
  @Output() public onRemove: EventEmitter<void> = new EventEmitter<void>();

  constructor(private router: Router) {}

  public removeBird() {
    this.onRemove.emit();
  }

  public editBird() {
    this.router.navigate(['create-bird'], { queryParams: {
      id: this.bird?.id
    }});
  }

  public navigateToDetail() {
    if (this.bird) {
    this.router.navigate(['detail', this.bird.id]);
    }
  }
}
