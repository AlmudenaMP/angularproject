import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BirdListComponent } from './bird-list.component';

const routes: Routes = [
  {
    path: '',
    component: BirdListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BirdListRoutingModule { }