import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MyCreationsDetailRoutingModule } from './my-creations-detail-routing.module';
import { MyCreationsDetailComponent } from './my-creations-detail.component';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
    MyCreationsDetailComponent,
  ],
  imports: [
    CommonModule,
    MyCreationsDetailRoutingModule,
    RouterModule
  ]
})
export class MyCreationsDetailModule { }
