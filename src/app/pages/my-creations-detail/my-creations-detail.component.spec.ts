import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyCreationsDetailComponent } from './my-creations-detail.component';

describe('MyCreationsDetailComponent', () => {
  let component: MyCreationsDetailComponent;
  let fixture: ComponentFixture<MyCreationsDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyCreationsDetailComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MyCreationsDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
