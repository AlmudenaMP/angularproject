import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Bird } from 'src/app/core/services/birds/birds.models';
import { BirdsService } from 'src/app/core/services/birds/birds.service';

@Component({
  selector: 'app-my-creations-detail',
  templateUrl: './my-creations-detail.component.html',
  styleUrls: ['./my-creations-detail.component.scss']
})
export class MyCreationsDetailComponent {

  public bird?: Bird

  constructor(
    private activatedRoute: ActivatedRoute,
    private birdsService: BirdsService,

  ) {
    this.activatedRoute.params.subscribe((params) => {
      const birdId = params['id'];
      this.birdsService.getNewBirdDetail(birdId).subscribe((bird) => {
        this.bird = bird;
      })
    });
  }
}
