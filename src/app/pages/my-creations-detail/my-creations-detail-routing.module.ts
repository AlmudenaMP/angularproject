import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MyCreationsDetailComponent } from './my-creations-detail.component';

const routes: Routes = [
  {
    path: '',
    component: MyCreationsDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyCreationsDetailRoutingModule { }
