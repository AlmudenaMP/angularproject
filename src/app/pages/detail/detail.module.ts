import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DetailRoutingModule } from './detail-routing.module';
import { DetailComponent } from './detail.component';
import { RouterModule } from '@angular/router';
import { ClasificationComponent } from './components/clasification/clasification.component';
import { DescriptionComponent } from './components/description/description.component';
import { OtherInfoComponent } from './components/other-info/other-info.component';


@NgModule({
  declarations: [
    DetailComponent,
    ClasificationComponent,
    DescriptionComponent,
    OtherInfoComponent,
  ],
  imports: [
    CommonModule,
    DetailRoutingModule,
    RouterModule
  ]
})
export class DetailModule { }
