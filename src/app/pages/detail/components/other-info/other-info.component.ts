import { Component, Input } from '@angular/core';
import { Bird } from 'src/app/core/services/birds/birds.models';


@Component({
  selector: 'app-other-info',
  templateUrl: './other-info.component.html',
  styleUrls: ['./other-info.component.scss']
})
export class OtherInfoComponent {

  @Input() public bird?: Bird
  
}
