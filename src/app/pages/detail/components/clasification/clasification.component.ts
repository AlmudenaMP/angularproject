import { Component, Input } from '@angular/core';
import { ApiBird } from 'src/app/core/services/birds/api/api-birds.models';
import { Bird } from 'src/app/core/services/birds/birds.models';

@Component({
  selector: 'app-clasification',
  templateUrl: './clasification.component.html',
  styleUrls: ['./clasification.component.scss']
})
export class ClasificationComponent {

  @Input() public bird?: Bird;

}
