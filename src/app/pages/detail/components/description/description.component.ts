import { Component, Input } from '@angular/core';
import { ApiBird } from 'src/app/core/services/birds/api/api-birds.models';
import { Bird } from 'src/app/core/services/birds/birds.models';

@Component({
  selector: 'app-description',
  templateUrl: './description.component.html',
  styleUrls: ['./description.component.scss']
})
export class DescriptionComponent {

  @Input() public bird?: Bird;

}
