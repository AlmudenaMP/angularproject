import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Bird } from 'src/app/core/services/birds/birds.models';
import { BirdsService } from 'src/app/core/services/birds/birds.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent {

  public bird?: Bird

  constructor(
    private activatedRoute: ActivatedRoute,
    private birdsService: BirdsService
  ) {
    this.activatedRoute.params.subscribe((params) => {
      const birdId = params['id'];
      this.birdsService.getBirdDetail(birdId).subscribe((bird) => {
        this.bird = bird;
      })
    });
  }

}
