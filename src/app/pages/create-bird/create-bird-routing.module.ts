import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateBirdComponent } from './create-bird.component';

const routes: Routes = [
  {
    path: '',
    component: CreateBirdComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreateBirdRoutingModule { }
