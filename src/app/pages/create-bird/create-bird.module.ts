import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreateBirdRoutingModule } from './create-bird-routing.module';
import { CreateBirdComponent } from './create-bird.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [
    CreateBirdComponent
  ],
  imports: [
    CommonModule,
    CreateBirdRoutingModule,
    ReactiveFormsModule,
    RouterModule
  ]
})
export class CreateBirdModule { }
