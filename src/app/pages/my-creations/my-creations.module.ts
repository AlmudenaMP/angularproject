import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MyCreationsRoutingModule } from './my-creations-routing.module';
import { MyCreationsComponent } from './my-creations.component';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { CreationComponent } from './creation/creation/creation.component';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [
    MyCreationsComponent,
    CreationComponent
  ],
  imports: [
    CommonModule,
    MyCreationsRoutingModule,
    FormsModule,
    SharedModule,
    RouterModule
  ]
})
export class MyCreationsModule { }
