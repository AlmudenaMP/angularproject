import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Bird } from 'src/app/core/services/birds/birds.models';

@Component({
  selector: 'app-creation',
  templateUrl: './creation.component.html',
  styleUrls: ['./creation.component.scss']
})
export class CreationComponent {

  @Input() public bird?: Bird;
  @Output() public onRemove: EventEmitter<void> = new EventEmitter<void>();

  constructor(private router: Router) {}

  public navigateToDetail() {
    if(this.bird) {
      this.router.navigate(['my-creations-detail', this.bird.id]);
    }
  }

  public removeBird() {
    this.onRemove.emit();
  }

  public editBird() {
    this.router.navigate(['create-new-bird'], { queryParams: {
      id: this.bird?.id
    }});
  }



}
