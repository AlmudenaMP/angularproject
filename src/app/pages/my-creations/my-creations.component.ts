import { Component, OnInit } from '@angular/core';
import { Bird } from 'src/app/core/services/birds/birds.models';
import { BirdsService } from 'src/app/core/services/birds/birds.service';
import { switchMap } from 'rxjs';

@Component({
  selector: 'app-my-creations',
  templateUrl: './my-creations.component.html',
  styleUrls: ['./my-creations.component.scss']
})
export class MyCreationsComponent {

    public birds: Bird[] = [];
    public birdCommonName : string = '';

    constructor(private birdsService: BirdsService) {}

    public ngOnInit(): void {
        this.birdsService.getNewBirds().subscribe((birdsFromApi) =>{
            this.birds = birdsFromApi;
        });
    }

    public removeNewBirdFromList(id?: string) {
        if (!id) {return; }
        this.birdsService.deleteNewBird(id).pipe(
            switchMap((bird) => {
                return this.birdsService.getNewBirds();
            })
        ).subscribe((birdsFromApi) => {
            this.birds = birdsFromApi;
        });
    }



}
