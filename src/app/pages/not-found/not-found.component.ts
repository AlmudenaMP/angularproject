import { Component } from '@angular/core';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss']
})
export class NotFoundComponent {

  public img ="https://culturacolectiva-cultura-colectiva-prod.cdn.arcpublishing.com/resizer/MqjGmOu9cMk2G6MI9UV8MxB9o1w=/600x450/filters:format(jpg):quality(70)/cloudfront-us-east-1.images.arcpublishing.com/culturacolectiva/MGQJO6WIJFF53J5URU6MLHVUA4.jpg"
}
