import { Pipe, PipeTransform } from '@angular/core';
import { Bird } from 'src/app/core/services/birds/birds.models';
import { Park } from 'src/app/core/services/parks/api/parks.models';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(value: Bird[] | null, commonName: string = '', seen?: Park): Bird[] {
    if (!value) {return[]; }
    if (!commonName && !seen) { return value; }
    return value.filter((bird) => {
      return bird.commonName.includes(commonName)
    });
  }
}
