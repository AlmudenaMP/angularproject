import { NationalPark } from "../../birds/api/api-birds.models";

export interface Park {

    name: NationalPark
    autonomousCommunity?: string;
    province?: string;
    declarationYear?: number;
    id?: string;
}