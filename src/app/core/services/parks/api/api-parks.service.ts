import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiPark } from './api-parks-models';


const BASE_URL = 'https://63e53e768e1ed4ccf6f0c3dc.mockapi.io/api/v1/nationalParks'


@Injectable({
  providedIn: 'root'
})
export class ApiParksService {

  constructor(private http: HttpClient) { }

  public getApiParks(): Observable<ApiPark[]> {
    return this.http.get<ApiPark[]>(BASE_URL)
  }
}
