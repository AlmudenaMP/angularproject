import { TestBed } from '@angular/core/testing';

import { ApiParksService } from './api-parks.service';

describe('ApiParksService', () => {
  let service: ApiParksService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApiParksService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
