import { Injectable } from '@angular/core';
import { ApiParksService } from './api/api-parks.service';
import { map, Observable } from 'rxjs';
import { Park } from './api/parks.models';
import { ApiPark } from './api/api-parks-models';

@Injectable({
  providedIn: 'root'
})
export class ParksService {

  constructor(private apiParksService: ApiParksService) { }

  public getParks(): Observable<Park[]> {
    return this.apiParksService.getApiParks().pipe(
      map((apiParks: ApiPark[]) => {
        return apiParks.map((apiPark) => ({
          id: apiPark.id,
          name: apiPark.name,
          autonomousCommunity: apiPark.autonomousCommunity,
          province: apiPark.province,
          declarationYear: apiPark.declarationYear
        }));
      })
    );
  }
}
