import { Park } from "../parks/api/parks.models";
import { ApiBird } from "./api/api-birds.models";
import { Bird } from "./birds.models";

export function transformBird(apiBird: ApiBird, selectedPark?: Park): Bird {
    return {
        ...apiBird,
        seen: selectedPark
            ? selectedPark
            : { name: apiBird.seen }
    };
}