import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiBird } from './api-birds.models';
import { Observable } from 'rxjs';
import { Bird } from '../birds.models';

const API_BIRD_URL = 'https://63e53e768e1ed4ccf6f0c3dc.mockapi.io/api/v1';
const API_NEWBIRD_URL = 'https://63fb4ee77a045e192b66c0b0.mockapi.io/api/v2';

@Injectable({
  providedIn: 'root'
})
export class ApiBirdsService {


  constructor(
    private http: HttpClient
  ) { }

  public getApiBirds(): Observable<ApiBird[]> {
    return this.http.get<ApiBird[]>(`${API_BIRD_URL}/birdsOfPrey`);
  }

  public getApiBirdDetail(id: string): Observable<ApiBird> {
    return this.http.get<ApiBird>(`${API_BIRD_URL}/birdsOfPrey/${id}`);
  }

  public getApiNewBirds(): Observable<ApiBird[]> {
    return this.http.get<ApiBird[]>(`${API_NEWBIRD_URL}/newBirds`);
  }

  public getApiNewBirdDetail(id: string): Observable<ApiBird> {
    return this.http.get<ApiBird>(`${API_NEWBIRD_URL}/newBirds/${id}`);
  }

  public deleteApiBird(id: string): Observable<ApiBird> {
    return this.http.delete<ApiBird>(`${API_BIRD_URL}/birdsOfPrey/${id}`);
  }

  public deleteApiNewBird(id: string): Observable<ApiBird> {
    return this.http.delete<ApiBird>(`${API_NEWBIRD_URL}/newBirds/${id}`);
  }

  public createApiBird(body: Bird): Observable<ApiBird> {
    return this.http.post<ApiBird>(`${API_NEWBIRD_URL}/newBirds`, body);
  }

  public createApiNewBird(body: Bird): Observable<ApiBird> {
    return this.http.post<ApiBird>(`${API_NEWBIRD_URL}/newBirds`, body);
  }

  public editApiBird(id: string, body: Bird): Observable<ApiBird> {
    return this.http.put<ApiBird>(`${API_BIRD_URL}/birdsOfPrey/${id}`, body);
  } 

  public editApiNewBird(id: string, body: Bird): Observable<ApiBird> {
    return this.http.put<ApiBird>(`${API_NEWBIRD_URL}/newBirds/${id}`, body);
  } 
}
