export interface ApiBird {
    family: string;
    binomialName: string;
    commonName: string;
    description: string;
    food: string;
    redList: string;
    seen: NationalPark;
    img: string;
    id: string;
}

export type NationalPark = 'Parque Nacional de Aigüestortes i Estany de Sant Maurici' | 'Parque Nacional Marítimo - Terrestre del Archipiélago de Cabrera' | 'Parque Nacional de Cabañeros' | 'Parque Nacional de la Caldera de Taburiente' | 'Parque Nacional de Doñana' | 'Parque Nacional de Garajonay' | 'Parque Nacional Marítimo-Terrestre de las Islas Atlánticas de Galicia' | 'Parque Nacional de Monfragüe' | 'Parque Nacional de Ordesa y Monte Perdido' | 'Parque Nacional de los Picos de Europa' | 'Parque Nacional de la Sierra de Guadarrama' | 'Parque Nacional de la Sierra de las Nieves' | 'Parque Nacional de las Tablas de Daimiel' | 'Parque Nacional de Sierra Nevada' | 'Parque Nacional del Teide' | 'Parque Nacional de Timanfaya';