import { Park } from "../parks/api/parks.models";

export interface Bird {
    family: string;
    binomialName: string;
    commonName: string;
    description: string;
    food: string;
    redList: string;
    seen: Park;
    img: string;
    id?: string;
}