import { Injectable } from '@angular/core';
import { ApiBirdsService } from './api/api-birds.service';
import { Observable, map, forkJoin, catchError } from 'rxjs';
import { Bird } from './birds.models';
import { ApiBird } from './api/api-birds.models';
import { transformBird } from './birds.helpers';
import { ParksService } from '../parks/parks.service';
import { Park } from '../parks/api/parks.models';


@Injectable({
  providedIn: 'root'
})
export class BirdsService {

  constructor(
    private apiBirdsService: ApiBirdsService,
    private parksService: ParksService
    ) { }

  public getBirds(): Observable<Bird[]> {
    return this.apiBirdsService.getApiBirds().pipe(
      map((birds: ApiBird[]) => {
        return birds.map((bird) => transformBird(bird));
      }),
    );
  }

  public getNewBirds(): Observable<Bird[]> {
    return this.apiBirdsService.getApiNewBirds().pipe(
      map((birds: ApiBird[]) => {
        return birds.map((bird) => transformBird(bird));
      }),
    );
  }

  public getBirdDetail(id: string):Observable<Bird> {
    return forkJoin([
      this.apiBirdsService.getApiBirdDetail(id),
      this.parksService.getParks().pipe(
        catchError((err) => {
          return [];
        })
      )
    ]).pipe(
      map(([apiBird, parks]) => {
        const selectedPark = parks.find((park) => park.name === apiBird.seen)
        return transformBird(apiBird, selectedPark)
      })
    );
  }
  
  public getNewBirdDetail(id: string):Observable<Bird> {
    return forkJoin([
      this.apiBirdsService.getApiNewBirdDetail(id),
      this.parksService.getParks().pipe(
        catchError((err) => {
          return [];
        })
      )
    ]).pipe(
      map(([apiBird, parks]) => {
        const selectedPark = parks.find((park) => park.name === apiBird.seen)
        return transformBird(apiBird, selectedPark)
      })
    );
  }

  public deleteBird(id: string): Observable<Bird> {
    return this.apiBirdsService.deleteApiBird(id).pipe(
      map((bird) => transformBird(bird))
    );
  }

  public deleteNewBird(id: string): Observable<Bird> {
    return this.apiBirdsService.deleteApiNewBird(id).pipe(
      map((bird) => transformBird(bird))
    );
  }

  public createBird(body: Bird): Observable<Bird> {
    return this.apiBirdsService.createApiBird(body).pipe(
      map((bird) => transformBird(bird))
    );
  }

  public createNewBird(body: Bird): Observable<Bird> {
    return this.apiBirdsService.createApiNewBird(body).pipe(
      map((bird) => transformBird(bird))
    );
  }

  public editBird(id: string, body: Bird): Observable<Bird> {
    return this.apiBirdsService.editApiBird(id, body).pipe(
      map((bird) => transformBird(bird))
    );
  }

  public editNewBird(id: string, body: Bird): Observable<Bird> {
    return this.apiBirdsService.editApiNewBird(id, body).pipe(
      map((bird) => transformBird(bird))
    );
  }
}
