import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then(m => m.HomeModule)
  },
  {
    path: 'bird-list',
    loadChildren: () => import('./pages/bird-list/bird-list.module').then(m => m.BirdListModule)
  },
  {
    path: 'detail/:id',
    loadChildren: () => import('./pages/detail/detail.module').then(m => m.DetailModule)
  },
  {
    path: 'create-bird',
    loadChildren: () => import('./pages/create-bird/create-bird.module').then(m => m.CreateBirdModule)
  },
  {
    path: 'my-creations',
    loadChildren: () => import('./pages/my-creations/my-creations.module').then(m => m.MyCreationsModule)
  },
  {
    path: 'my-creations-detail/:id',
    loadChildren: () => import('./pages/my-creations-detail/my-creations-detail.module').then(m => m.MyCreationsDetailModule)
  },
  {
    path: 'create-new-bird',
    loadChildren: () => import('./pages/create-new-bird/create-new-bird.module').then(m => m.CreateNewBirdModule)
  },
  {
    path: 'about-me',
    loadChildren: () => import('./pages/about-me/about-me.module').then(m => m.AboutMeModule)
  },
  {
    path: '**',
    loadChildren: () => import('./pages/not-found/not-found.module').then(m => m.NotFoundModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
